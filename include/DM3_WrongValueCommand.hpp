/* Copyright 2018 LAAS-CNRS
*
* This file is part of the OSMOSIS project.
*
* Osmosis is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 3 as
* published by the Free Software Foundation.
*
* Osmosis is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
*/

#ifndef OSMOSIS_DM3_WrongValueCommand_HPP
#define OSMOSIS_DM3_WrongValueCommand_HPP

#include <iostream>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

#include "DetectionModule.hpp"

class DM3_WrongValueCommand : public DetectionModule
{
private:
	///////// Attributes ////////
	ros::Subscriber cmd_sub_;
	geometry_msgs::Twist cmd_;
	float cmd_linear_x_max_;
	float cmd_linear_x_min_;
	float cmd_angular_z_max_;
	float cmd_angular_z_min_;

	ros::NodeHandle nh_;
	double max_linear;
	double max_angular;
	///////// Methods ////////
	void detect();

public:
	DM3_WrongValueCommand();
	void init();
	void DM3_WrongValueCommandCallback(const geometry_msgs::Twist & cmd_msg);

}; // end of class

#endif //OSMOSIS_DM3_WrongValueCommand_HPP
